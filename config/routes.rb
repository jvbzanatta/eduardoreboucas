Rails.application.routes.draw do

  get '/' => 'venues#index'
  
  devise_for :users

  get 'venues/save_query' => 'venues#save_query'

  resources :venues
  
  post 'venues/:id/rate' => 'venues#rate'

  get '/ratings/:id' => 'venues#rating'

  get '/welcome' => 'welcome#index'

  get '/my_ratings' => 'my_ratings#index'

  get "/about" => 'about#index'
  
end
