class MyRatingsController < ApplicationController

  before_action :authenticate_user!

  def index

    @ratings = current_user.ratings.order('updated_at desc')
    
  end

end
