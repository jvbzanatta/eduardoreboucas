class VenuesController < ApplicationController
  
  before_action :authenticate_user!

  before_action :set_venue, only: [:show, :edit, :update, :destroy, :rate]

  # GET /venues
  def index
    @can_search = true
    @venues = Venue.all.limit(20)
    if cookies[:distance] && cookies[:distance] != 'Todos'
      @filtered = true
    end    
    if cookies[:venue_type] && cookies[:venue_type] != 'Todos'
      @filtered = true
      @venues = @venues.where(venue_type: cookies[:venue_type])
    end
    if cookies[:price_rating] && cookies[:price_rating] != 'Todos'
      @filtered = true
      @venues = @venues.where('price_rating > 0 and price_rating <= ?', cookies[:price_rating])
    end
    unless cookies[:q].blank?
      @filtered = true
      q = "%#{cookies[:q]}%"
      @venues = @venues.where("city like ? or state like ? or country like ?", q, q, q)
    end
  end

  # GET /venues/1
  def show
  end

  # GET /venues/new
  def new
    @venue = Venue.new
  end

  # GET /venues/1/edit
  def edit
  end

  # POST /venues
  def create
    @venue = Venue.new(venue_params)

    if @venue.save
      redirect_to @venue, notice: 'Obrigado por compartilhar mais um local conosco!.'
    else
      render :new
    end
  end

  # PATCH/PUT /venues/1
  def update
    if @venue.update(venue_params)
      redirect_to venues_url, notice: 'Venue was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /venues/1
  def destroy
    @venue.destroy
    redirect_to venues_url, notice: 'Venue was successfully destroyed.'
  end

  #POST
  def rate
    @rating = Rating.new(rating_params)
    @rating.venue_id = params[:id]
    @rating.user = current_user
    if @rating.save
      @venue.update_ratings
      redirect_to venues_url, notice: "Obrigado por sua contribuição!"
    end
  end

  #POST
  def rating
    @rating = Rating.find(params[:id])
    @venue = @rating.venue

    render 'show'
  end

  #GET
  def save_query
    cookies[:q] = params[:q]
    cookies[:price_rating] = params[:price_rating]
    cookies[:venue_type] = params[:venue_type]
    cookies[:distance] = params[:distance]
    redirect_to venues_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_venue
      @venue = Venue.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def venue_params
      params.require(:venue).permit(:venue_name, :venue_type, :address, :city, :state, :country, :network_name, :network_is_free, :network_password)
    end

    def rating_params
      params.require(:rating).permit(:foods, :drinks, :service_rating, :price_rating, :comfort_rating, :noise_rating, :overall_rating)
    end

end
