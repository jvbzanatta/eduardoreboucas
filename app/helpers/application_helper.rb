module ApplicationHelper

  def render_evaluation(value)
    html = ''
    (1..5).each do |i|
      if (value >= i)
        html += "<i class='fa fa-star'></i>"
      else
        html += "<i class='fa fa-star-o'></i>"
      end
    end
    html.html_safe
  end

  def input_evaluation(element)
    html = "<input type='hidden' name='#{element}' value='0'>"
    (1..5).each do |i|
      html += "<i class='fa fa-star-o rating-star' data-element='#{element}' data-value='#{i}'></i>"
    end
    html.html_safe
  end

end
