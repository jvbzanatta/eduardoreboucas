class Venue < ApplicationRecord

  has_many :ratings

  def full_address
    [self.address, self.city, self.state, self.country].compact.join(', ')
  end

  def update_ratings
    tags = %w{service price comfort noise overall}
    count = 0
    tags.each { |tag| self.send("#{tag}_rating=", 0)}
    self.ratings.each do |rating|
      tags.each { |tag| self.send("#{tag}_rating=", self.send("#{tag}_rating") + rating.send("#{tag}_rating"))  }
      count += 1
    end
    if count > 0
      tags.each { |tag| self.send("#{tag}_rating=", self.send("#{tag}_rating").to_f / count) }
    end
    self.rating_count = count
    self.save
  end

end
