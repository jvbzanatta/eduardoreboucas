# Tem Wifi?

## Escopo

  1o dia
  - Escolha de template (2h)
  - Menu (1h)
  - Sobre (1h)
  - Instalação no servidor (2h)

  2o dia
  - Cadastro de avaliações (4h)
  - Minhas avaliações (4h)

  3o dia
  - Listagem de avaliações (8h)

  4o dia
  - Visualização de avaliações (4h)
  - Cadastro/Login (4h)

  5o dia
  - Acertos após avaliação do cliente (8h)

## Prazo

  5 (cinco) dias úteis

## Atividades realizadas

  Todas as atividades do escopo foram feitas com exceção:

  - Instalação do Servidor - decidiu-se fazer um vídeo explicativo
  - Login com Facebook e Google - por não se ter feito a instalação do servidor
  - Acertos - Seria a etapa final após apresentação com o cliente

  Vide detalhamento no arquivo doc/controle_horas.pdf

## Apresentação

https://youtu.be/-RaaA0A61rA