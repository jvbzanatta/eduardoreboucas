class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.references :venue
      t.references :user
      t.string :foods
      t.string :drinks
      t.integer :service_rating, default: 0
      t.integer :price_rating, default: 0
      t.integer :comfort_rating, default: 0
      t.integer :noise_rating, default: 0
      t.integer :overall_rating, default: 0
      t.timestamps      
    end
  end
end
