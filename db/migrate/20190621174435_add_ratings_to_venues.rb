class AddRatingsToVenues < ActiveRecord::Migration[5.2]
  def change
    add_column :venues, :service_rating, :decimal, precision: 5, scale: 2, default: 0
    add_column :venues, :price_rating, :decimal, precision: 5, scale: 2, default: 0
    add_column :venues, :comfort_rating, :decimal, precision: 5, scale: 2, default: 0
    add_column :venues, :noise_rating, :decimal, precision: 5, scale: 2, default: 0
    add_column :venues, :overall_rating, :decimal, precision: 5, scale: 2, default: 0
    add_column :venues, :rating_count, :integer, default: 0
  end
end
