class CreateVenues < ActiveRecord::Migration[5.2]
  def change
    create_table :venues do |t|
      t.string :venue_name
      t.string :venue_type
      t.string :address
      t.string :city
      t.string :state
      t.string :country
      t.string :network_name
      t.boolean :network_is_free
      t.string :network_password
      t.timestamps
    end
  end
end
