# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_21_174435) do

  create_table "ratings", force: :cascade do |t|
    t.integer "venue_id"
    t.integer "user_id"
    t.string "foods"
    t.string "drinks"
    t.integer "service_rating", default: 0
    t.integer "price_rating", default: 0
    t.integer "comfort_rating", default: 0
    t.integer "noise_rating", default: 0
    t.integer "overall_rating", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_ratings_on_user_id"
    t.index ["venue_id"], name: "index_ratings_on_venue_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "venues", force: :cascade do |t|
    t.string "venue_name"
    t.string "venue_type"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "network_name"
    t.boolean "network_is_free"
    t.string "network_password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "service_rating", precision: 5, scale: 2, default: "0.0"
    t.decimal "price_rating", precision: 5, scale: 2, default: "0.0"
    t.decimal "comfort_rating", precision: 5, scale: 2, default: "0.0"
    t.decimal "noise_rating", precision: 5, scale: 2, default: "0.0"
    t.decimal "overall_rating", precision: 5, scale: 2, default: "0.0"
    t.integer "rating_count", default: 0
  end

end
