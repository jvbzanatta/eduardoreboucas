require "application_system_test_case"

class VenuesTest < ApplicationSystemTestCase
  setup do
    @venue = venues(:one)
  end

  test "visiting the index" do
    visit venues_url
    assert_selector "h1", text: "Venues"
  end

  test "creating a Venue" do
    visit venues_url
    click_on "New Venue"

    fill_in "Address", with: @venue.address
    fill_in "City", with: @venue.city
    fill_in "Country", with: @venue.country
    check "Network is free" if @venue.network_is_free
    fill_in "Network name", with: @venue.network_name
    fill_in "Network password", with: @venue.network_password
    fill_in "State", with: @venue.state
    fill_in "Venue name", with: @venue.venue_name
    fill_in "Venue type", with: @venue.venue_type
    click_on "Create Venue"

    assert_text "Venue was successfully created"
    click_on "Back"
  end

  test "updating a Venue" do
    visit venues_url
    click_on "Edit", match: :first

    fill_in "Address", with: @venue.address
    fill_in "City", with: @venue.city
    fill_in "Country", with: @venue.country
    check "Network is free" if @venue.network_is_free
    fill_in "Network name", with: @venue.network_name
    fill_in "Network password", with: @venue.network_password
    fill_in "State", with: @venue.state
    fill_in "Venue name", with: @venue.venue_name
    fill_in "Venue type", with: @venue.venue_type
    click_on "Update Venue"

    assert_text "Venue was successfully updated"
    click_on "Back"
  end

  test "destroying a Venue" do
    visit venues_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Venue was successfully destroyed"
  end
end
